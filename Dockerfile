FROM ruby:2.3.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /sc_auth_app
WORKDIR /sc_auth_app
ADD Gemfile /sc_auth_app/Gemfile
ADD Gemfile.lock /sc_auth_app/Gemfile.lock
RUN bundle install
ADD . /sc_auth_app
